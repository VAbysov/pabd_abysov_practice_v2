import os
import click
import pandas as pd
import yaml

from datetime import datetime
from yaml import SafeLoader
from joblib import load
from sklearn.metrics import mean_squared_error
from path_changer import path


def load_config(config):
    with open(config) as f:
        config = yaml.load(f, Loader=SafeLoader)
    data_path = config['data_path']
    model_path = config['model_path']
    report_path = config['test_report_path']
    return data_path, model_path, report_path


def generate_test_report(X_test, y_test, df, model, report_path):
    r2 = model.score(X_test, y_test)
    y_true = y_test
    y_pred = model.predict(X_test)
    rmse = mean_squared_error(y_true, y_pred, squared=False)
    print("Coef: ", model.coef_)
    report = [
        f'Time: {datetime.now()}\n',
        f'Test data len: {len(df)}\n',
        f'R2: {r2}\n'
        f'RMSE: {round(rmse)} RUB\n'
    ]
    with open(report_path, 'w') as f:
        f.writelines(report)


@click.command()
@click.option('--config', default='params/evaluate.yaml')
def evaluate(config):
    data_path, model_path, report_path = load_config(config)

    df = pd.read_csv(data_path)
    X_test = df['total_meters'].to_numpy().reshape(-1, 1)
    y_test = df['price']
    model = load(model_path)
    generate_test_report(X_test, y_test, df, model, report_path)


if __name__ == '__main__':
    os.chdir(path)
    evaluate()
