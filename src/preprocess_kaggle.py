import os
import click
import yaml
import pandas as pd

from sklearn.model_selection import train_test_split
from path_changer import path


def square_foots_to_meters(foots: int) -> int:
    return round(foots * 0.092903)


def usd_to_rub(usd: int) -> int:
    return int(usd * 82 / 1000)


@click.command()
@click.option('--in_data', default='data/kaggle/train.csv')
@click.option('--out_data', default='data/processed/kaggle/train.csv')
@click.option('--config', default='params/preprocess_kaggle.yml')
def preprocess_data(in_data, out_data, config):
    with open(config) as f:
        config = yaml.safe_load(f)

    df = pd.read_csv(in_data)
    new_df = df[['GrLivArea', 'SalePrice']]

    new_df.loc[:, 'GrLivArea'] = new_df['GrLivArea'].apply(square_foots_to_meters)
    new_df.loc[:, 'SalePrice'] = new_df['SalePrice'].apply(usd_to_rub)
    name_mapper = {
        'GrLivArea': 'total_meters',
        'SalePrice': 'price'
    }
    new_df = new_df.rename(columns=name_mapper)
    print(new_df.head())

    train_df, test_df = train_test_split(new_df, test_size=0.2)
    train_df.to_csv(config['train_out'])
    test_df.to_csv(config['test_out'])


if __name__ == '__main__':
    os.chdir(path)
    preprocess_data()
