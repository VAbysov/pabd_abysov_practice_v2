"""House price prediction service"""
import os
import yaml

from path_changer import path
from joblib import load
from flask import Flask, request, render_template
from yaml import SafeLoader
from waitress import serve


os.chdir(path)
app = Flask(__name__)
config = yaml.load(open('params/predict_app.yaml'), SafeLoader)
model = load(config['model_path'])


@app.route("/")
def home():
    return render_template('prec_template.html')


@app.route('/predict', methods=['GET'])
def predict():
    f_1 = request.args.get("area")
    result = model.predict([[int(f_1)]])
    return f'{round(result[0] / 1000)} тыс. руб.'


if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=3000)
