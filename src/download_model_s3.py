import os
import boto3

from path_changer import path


def download_model(name):
    session = boto3.session.Session()
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
    )
    s3.download_file(name, 'model_221716.joblib', 'models/model_221716.joblib')


if __name__ == '__main__':
    os.chdir(path)
    bucket_name = 'pabd-s3'
    download_model(bucket_name)


