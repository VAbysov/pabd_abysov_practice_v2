import unittest

from requests import request


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.url = 'http://localhost:3000/'
        self.endpoint = 'predict'

    def test_home_page(self):
        response = request('GET', self.url)
        self.assertIn('House Price Prediction', response.text)

    def test_prediction(self):
        response = request('GET', self.url+self.endpoint, params={'area': 100})
        self.assertIn('тыс. руб.', response.text)
