FROM python:3.11
WORKDIR /pabd_abysov_practice_v2
COPY ./src ./src
COPY ./models/model_221716.joblib ./models/model_221716.joblib
RUN pip install -r requirements.txt
EXPOSE 3000
CMD ["python", "./src/predict_app.py"]

